﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Rechnungen
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public string customerID { get; set; }
        public string customerName { get; set; }
        public string customerStreet { get; set; }
        public string customerPlz { get; set; }
        public string customerCity { get; set; }

        public MainWindow()
        {
            InitializeComponent();
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            customerID =  ID.Text;
            customerName = name.Text;
            customerStreet = street.Text;
            customerPlz =  plz.Text;
            customerCity = city.Text;

            List<string> KundeListe = new List<string>();
            // nach klasse mit Kunde und statt string klasse Kunde
            KundeListe.Add(customerID);
            KundeListe.Add(customerName);
            KundeListe.Add(customerStreet);
            KundeListe.Add(customerPlz);
            KundeListe.Add(customerCity);



            showid.Content = KundeListe[0];
            showname1.Content = KundeListe[1];
            showstreet.Content = KundeListe[2];
            showplz.Content = KundeListe[3];
            showort.Content = KundeListe[4];


        }

        private void newcustom_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
